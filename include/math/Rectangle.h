#ifndef INCLUDE_MATH_RECTANGLE_H_
#define INCLUDE_MATH_RECTANGLE_H_

struct Rectangle
{
	Rectangle(int width, int height);

	int getSquare() const;

	int getPerimeter() const;

private:
	 int width;
	 int height;
};




#endif 
