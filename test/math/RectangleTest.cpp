#include <gtest/gtest.h>
#include "math/Rectangle.h"

struct RectangleTest : testing::Test
{
	RectangleTest() : rectangle(3, 4)
	{
	}

protected:
    Rectangle rectangle;
};

TEST_F(RectangleTest, should_be_able_to_get_square)
{
   ASSERT_EQ(12, rectangle.getSquare());
}

TEST_F(RectangleTest, should_be_able_to_get_perimeter)
{
   ASSERT_EQ(14, rectangle.getPerimeter());
}
