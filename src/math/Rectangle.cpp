#include "math/Rectangle.h"

Rectangle::Rectangle(int width, int height)
  : width(width), height(height)
{
}

int Rectangle::getSquare() const
{
	return width * height;
}

int Rectangle::getPerimeter() const
{
    return (width + height)*2;
}
